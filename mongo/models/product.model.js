const mongoose = require('mongoose')
const { Schema } = mongoose

const ProductSchema = new Schema(
    {
        name: { type: String, lowercase: true, es_type: 'text' },
        designer: { type: String, es_indexed: false },
        accountId: { type: String, es_indexed: false, required: true },
        sku: { type: String, trim: true, lowercase: true, es_indexed: false },
        code: { type: String, trim: true, uppercase: true, es_type: 'text' },
        type: { type: Schema.ObjectId, ref: 'Category', es_indexed: false }, //category as parent, ie, root category of hierarchy
        category: { type: Schema.ObjectId, ref: 'Category', es_indexed: false }, //category/subcategory/sub_subcategory, ie, immediate parent of product
        dimension: { es_indexed: false },
        material: {
            type: String,
            trim: true,
            lowercase: true,
            es_indexed: false
        },
        style: { type: String, trim: true, lowercase: true, es_indexed: false },
        colors: {
            type: Array,
            es_indexed: false
        },
        weight: {
            type: String,
            trim: true,
            lowercase: true,
            es_indexed: false
        },
        digitized: {
            type: String,
            trim: true,
            lowercase: true,
            es_indexed: false
        },
        price: Number,
        price_unit: {
            type: String,
            trim: true,
            lowercase: true,
            es_indexed: false
        },
        details: {
            type: String,
            trim: true,
            lowercase: true,
            es_indexed: false
        },
        description: {
            type: String,
            trim: true,
            lowercase: true,
            es_indexed: false
        },
        properties: {
            type: Object,
            default: {
                threeD: false,
                isPublic: true
            }
        },
        accessAccounts: { type: [String], default: [] },
        tags: { type: [], es_indexed: false },
        extraTags: { type: Array, default: [], es_indexed: false },
        regions: { type: Array, default: [], es_indexed: false },
        spaces: { type: Array, default: [], es_indexed: false },
        brand: { type: Schema.Types.Mixed, ref: 'Brand', es_indexed: false},
        searchtags: { type: String, es_indexed: false },
        threedmax: { type: String, trim: true, es_indexed: false },
        images: {
            type: [{ type: String, es_indexed: false }],
            es_indexed: false
        },
        sub_product: {
            type: [
                {
                    subproducts: {
                        type: [
                            {
                                type: Schema.ObjectId,
                                ref: 'Product',
                                es_indexed: false
                            }
                        ],
                        es_indexed: false
                    },
                    product_type: { type: String, es_indexed: false },
                    category: {
                        type: Schema.ObjectId,
                        ref: 'Category',
                        es_indexed: false
                    },
                    quantity: { type: Number, es_indexed: false },
                    dimension: { es_indexed: false },
                    es_indexed: false
                }
            ],
            es_indexed: false
        },
        related_product: {
            type: [
                {
                    type: Schema.ObjectId,
                    ref: 'Product',
                    related_type: { type: String, es_indexed: false },
                    related_code: { type: String, es_indexed: false },
                    es_indexed: false
                }
            ],
            es_indexed: false
        },
        related_product_obj: {
            type: [
                {
                    related_type: { type: String, es_indexed: false },
                    related_code: { type: String, es_indexed: false }
                }
            ],
            es_indexed: false
        },
        location: { type: String, es_indexed: false },
        vendor: { type: String, trim: true, es_indexed: false },
        widget: { type: Schema.ObjectId, ref: 'Widget', es_indexed: false },
        trends: {
            type: [
                {
                    id: { type: String, trim: true, es_indexed: false },
                    itemName: { type: String, trim: true, es_indexed: false }
                }
            ],
            es_indexed: false
        },
        trendDate: {
            startdate: { type: Date, default: null, es_indexed: false },
            enddate: { type: Date, default: null, es_indexed: false },
            es_indexed: false
        },
        track: {
            type: [{ type: Schema.ObjectId, es_indexed: false }],
            es_indexed: false
        },
        createdBy: { type: String, es_indexed: false, required: true },
        project: { type: String, es_indexed: false },
        updatedBy: { type: String, es_indexed: false, required: true },
        isDeleted: { type: Boolean, default: false, es_indexed: false },
        isOptimized: { type: Boolean, default: false, es_indexed: false },
        totaldownloads: { type: Number, default: 0, es_indexed: false },
        isAlgoliaIndexed: { type: Boolean, default: false, es_indexed: false },
        hideFromAlgolia: { type: Boolean, default: false, es_indexed: false },
        optimizedImages: { type: Array, default: [], es_indexed: false },
        isLinkedToCustomModel: { type: Boolean, default: false }
    },
    { versionKey: false, strict: false, timestamps: true }
)
module.exports = mongoose.model('Product', ProductSchema)
