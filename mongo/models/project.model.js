const mongoose = require('mongoose')
const {
    Schema
} = mongoose
const ProjectSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true,
        lowercase: true
    },
    createdBy: {
        type: String,
        ref: 'User',
        required: true,
        default: ''
    },
    updatedBy: {
        type: String,
        ref: 'User',
        required: true,
        default: ''
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
    published: {
        type: Boolean,
        default: false
    },
    properties: {
        type: Object,
        default: {}
    },
    jsondata: {
        type: Object,
        default: {}
    },
    type: {
        type: Array,
        default: ['project']
    },
    accountId: {
        type: String,
        required: true
    },
    lastSyncedDate: {
        type: Date
    },
    isLinkedToInspiration: {
        type: Boolean,
        default: false
    },
    inspiration: {
        type: String,
        ref: 'Product'
    },
    clonedFrom: {
        type: String
    },
    isSampleProject: {
        type: Boolean,
        default: false
    }
}, {
    versionKey: false,
    strict: true,
    timestamps: true
})

ProjectSchema.index({
    name: 1,
    accountId: 1
}, {
    unique: true
})
ProjectSchema.index({
    createdAt: 1
})
ProjectSchema.index({
    isDeleted: 1,
    published: 1
})
ProjectSchema.index({
    accountId: 1,
    isDeleted: 1,
    createdAt: -1
})

module.exports = mongoose.model('FPProject', ProjectSchema)