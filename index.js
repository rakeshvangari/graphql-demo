const { ApolloServer } = require("apollo-server");
const typeDefs = require('./graphql/schema')
const resolvers = require('./graphql/resolvers')
const mongoose = require('mongoose')

const server = new ApolloServer({
  typeDefs,
  resolvers
});

async function startServer() {

  await mongoose.connect('mongodb+srv://productmasterdev:u0h0d17l8tPma9uF@cluster0-xylcg.mongodb.net/productmaster_test', { useNewUrlParser: true, useUnifiedTopology: true })

  server.listen({ port: 4001 }).then(({ url }) => {
    console.log(`Server ready at ${url}`);
  });

}

startServer()
