const product = `name: String
    designer: String
    accountId: String
    sku: String
    code: String
    category: String
    style: String
    accessAccounts: String
    brand: String
    searchtags: String
    threedmax: String
    product_type: String
    quantity: Int
    related_type: String
    related_code: String
    location: String
    vendor: String
    widget: String
    id: String
    itemName: String
    startdate: String
    enddate: String
    type: String 
    createdBy: String
    project: String
    updatedBy: String
    isDeleted: Boolean
    isOptimized: Boolean
    totaldownloads: Int
    isAlgoliaIndexed: Boolean
    hideFromAlgolia: Boolean
    isLinkedToCustomModel: Boolean`

module.exports = product