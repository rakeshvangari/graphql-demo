const { gql } = require("apollo-server");
const product = require('./schema/product.schema')

const typeDefs = gql`
  type Query {
    getAllProducts(limit: Int): [Product]
    searchProductById(_id: ID!): Product
    getAllProductsOfProject(_id: ID!): [Product]
  }

  type Product {
    ${product}
  }

`;

module.exports = typeDefs