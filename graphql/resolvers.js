const productModel = require('../mongo/models/product.model')
const projectModel = require('../mongo/models/project.model')

const getProductsFromElementMap = (elementMap, extractKey) => {
    const productsArray = []
    const keys = Object.keys(elementMap)
    keys.forEach(x => {
        productsArray.push(elementMap[x][extractKey])
    })
    return productsArray
}

const resolvers = {
    Query: {
        async searchProductById(_, arguments) {
            let data = await productModel.findOne(arguments)
            return data;
        },
        async getAllProducts(_, arguments) {
            let data = await productModel.find().limit(arguments.limit || 10)
            return data;
        },
        async getAllProductsOfProject(_, arguments) {
            let project = await projectModel.findOne({ _id: arguments._id })
            let products = getProductsFromElementMap(project.jsondata.uniqueElementMap, "sourceProductID")
            let data = await productModel.find({ _id: { $in: products } })
            return data;
        }
    }
};

module.exports = resolvers